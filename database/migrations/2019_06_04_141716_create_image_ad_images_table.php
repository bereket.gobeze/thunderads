<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageAdImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_ad_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image_location');
            $table->string('size');
            $table->unsignedBigInteger('image_ad_id');
            $table->timestamps();

            $table->foreign('image_ad_id')->references('id')->on('image_ads');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_ad_images');
    }
}
