<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelegramChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegram_channels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('channel_name')->nullable();
            $table->string('channel_id')->unique()->nullable();
            $table->unsignedBigInteger('ad_unit_id');
            $table->timestamps();

            $table->foreign('ad_unit_id')->references('id')->on('ad_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telegram_channels');
    }
}
