<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTargetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_targetings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->text('gender');
            $table->unsignedBigInteger('content_id');
            $table->unsignedBigInteger('content_category_id');
            $table->integer('age_range_min');
            $table->integer('age_range_max');

            $table->foreign('content_id')->references('id')->on('contents');
            $table->foreign('content_category_id')->references('id')->on('content_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_targetings');
    }
}
