<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdClicksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_clicks', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('ad_id');
            $table->unsignedBigInteger('content_id');
            $table->unsignedBigInteger('ad_unit_id');

            $table->timestamps();

            $table->foreign('ad_id')->references('id')->on('ads');
            $table->foreign('content_id')->references('id')->on('contents');
            $table->foreign('ad_unit_id')->references('id')->on('ad_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_clicks');
    }
}
