<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdClick extends Model
{
    //
    protected $fillable = [
        'ad_id', 'content_id', 'ad_unit_id'
    ];

    public function ad() {
        return $this->belongsTo('App\Ad');
    }

    public function content() {
        return $this->belongsTo('App\Content');
    }

    public function adUnit() {
        return $this->belongsTo('App\AdUnit');
    }
}
