<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageAdImage extends Model
{
    //
    protected $fillable = [
        'image_location', 'size', 'image_ad_id'
    ];

    public function imageAd() {
        return $this->belongsTo('App\ImageAd');
    }
}
