<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    // User types
    public const USER_TYPE_PUBLISHER = 'publisher';
    public const USER_TYPE_ADVERTISER = 'advertiser';
    public const USER_TYPE_SUPER_ADMIN = 'super_admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'user_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function advertiser() {
        return $this->hasOne('App\Advertiser');
    }

    public function publisher() {
        return $this->hasOne('App\Publisher');
    }

    public function systemAdmin() {
        return $this->hasOne('App\SystemAdmin');
    }
}
