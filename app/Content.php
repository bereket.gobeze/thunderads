<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    //
    protected $fillable = [
        'name', 'type', 'location', 'publisher_id'
    ];

    public function contentTargeting() {
        return $this->hasOne('App\ContentTargeting');
    }

    public function publisher() {
        return $this->belongsTo('App\Publisher');
    }

    public function adUnits() {
        return $this->hasMany('App\AdUnit');
    }

    public function adViews() {
        return $this->hasMany('App\AdView');
    }

    public function adClicks() {
        return $this->hasMany('App\AdClick');
    }
}
