<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdUnit extends Model
{
    //
    protected $fillable = [
        'name', 'type', 'content_id', 'size'
    ];

    public function content(){
        return $this->belongsTo('App\Content');
    }

    public function adViews() {
        return $this->hasMany('App\AdView');
    }

    public function adClicks() {
        return $this->hasMany('App\AdClick');
    }

    public function telegramChannel() {
        return $this->hasOne('App\TelegramChannel');
    }
}
