<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentCategory extends Model
{
    //
    protected $fillable = [
        'name', 'description'
    ];

    public function contentTargeting() {
        return $this->hasMany('App\ContentTargeting');
    }
}
