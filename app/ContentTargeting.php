<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentTargeting extends Model
{
    //
    protected $fillable = [
        'gender', 'content_category_id', 'age_range_min', 'age_range_max'
    ];

    public function content(){
        return $this->belongsTo('App\Content');
    }

    public function contentCategory() {
        return $this->belongsTo('App\ContentCategory');
    }
}
