<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertiser extends Model
{
    //
    protected $fillable = [
        'balance', 'user_id'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function ads() {
        return $this->hasMany('App\Ad');
    }

    public function adClicks() {
        return $this->hasManyThrough('App\AdClick', 'App\Ad');
    }

    public function adViews() {
        return $this->hasManyThrough('App\AdView', 'App\Ad');
    }
}
