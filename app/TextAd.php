<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TextAd extends Model
{
    //
    protected $fillable = [
        'destination_url', 'headline', 'ad_text_line_one', 'ad_text_line_two', 'action_text', 'ad_id'
    ];

    public function ad() {
        return $this->belongsTo('App\Ad');
    }
}
