<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TelegramChannel extends Model
{
    //
    protected $fillable = [
        'channel_name', 'channel_id', 'ad_unit_id'
    ];

    public function adUnit() {
        return $this->belongsTo('App\AdUnit');
    }
}
