<?php

namespace App\Console\Commands;

use App\ContentCategory;
use Illuminate\Console\Command;

class AddCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $cC = new ContentCategory();
        $cC->name = 'Auto and Vehicles';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Beauty and References';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Business';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Tools';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Communication';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Dating';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Education';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Entertainment';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Events';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Finance';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Food and Drink';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Medical';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Music and Audio';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'News';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Photography';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Shopping';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Sports';
        $cC->description = '';
        $cC->save();

        $cC = new ContentCategory();
        $cC->name = 'Travel';
        $cC->description = '';
        $cC->save();
    }
}
