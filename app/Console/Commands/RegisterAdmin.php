<?php

namespace App\Console\Commands;

use App\SystemAdmin;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class RegisterAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:admin {name} {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register a new user with system admin role';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userCount = User::where('email', $this->argument('email'))->count();

        if ($userCount > 0) {
            $this->error("Email already taken.");
            return;
        }

        // Register the new user
        $user = new User();
        $user->name = $this->argument('name');
        $user->email = $this->argument('email');
        $user->password = Hash::make($this->argument('password'));
        $user->user_type = 'admin';
        $user->save();

        // Create the system admin
        $admin = new SystemAdmin();
        $admin->user_id = $user->id;
        $admin->save();

        $this->info("System Admin Registered Successfully");
    }
}
