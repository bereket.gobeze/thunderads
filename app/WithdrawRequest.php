<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WithdrawRequest extends Model
{
    protected $fillable = [
        'amount', 'status', 'publisher_id'
    ];


    public function publisher() {
        return $this->belongsTo('App\Publisher');
    }
}
