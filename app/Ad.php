<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    //
    protected $fillable = [
        'name', 'gender', 'age_range_min', 'age_range_max', 'type', 'advertiser_id'
    ];

    public function advertiser(){
        return $this->belongsTo('App\Advertiser');
    }

    public function textAd() {
        return $this->hasOne('App\TextAd');
    }

    public function imageAd() {
        return $this->hasOne('App\ImageAd');
    }

    public function adViews() {
        return $this->hasMany('App\AdView');
    }

    public function adClicks() {
        return $this->hasMany('App\AdClick');
    }
}
