<?php

namespace App\Http\Controllers;

use App\Ad;
use App\AdClick;
use App\AdUnit;
use App\AdView;
use App\ImageAdImage;
use App\TextAd;
use Illuminate\Http\Request;

class AdController extends Controller
{
    //
    public function textAdPreview(Request $request) {
        // Create ad object
        $ad = new \stdClass();
        $ad->type = 'text';

        // Create a Text Ad Object
        $textAd = new \stdClass();

        // Set the properties from the request
        $textAd->headline = $request->input('headline');
        $textAd->ad_text_line_one = $request->input('ad_text_line_one');
        $textAd->ad_text_line_two = $request->input('ad_text_line_two');
        $textAd->action_text = $request->input('action_text');
        $textAd->destination_url = $request->input('destination_url');

        // Get the view that is going to be displayed based on the ad size
        $adView = null;

        switch ($request->input('size')) {
            case '320x50':
                $adView = view('ads.textAds.mobile-banner');
                break;
            case '320x100':
                $adView = view('ads.textAds.large-mobile-banner');
                break;
            case '728x90':
                $adView = view('ads.textAds.leader-board');
                break;
            case '336x280':
                $adView = view('ads.textAds.large-rectangle');
                break;
            case '300x250':
                $adView = view('ads.textAds.medium-rectangle');
                break;
            case '300x600':
                $adView = view('ads.textAds.sky-scraper');
                break;
            default:
                $adView = view('ads.textAds.large-rectangle');
        }

        $ad->textAd = $textAd;
        $ad->click_url = 'https://google.com';


        return $adView->with('ad', $ad);
    }

    public function getAd(Request $request) {
        // validate the ad unit
        if (!$request->input('ad-unit')) {
            return "<h5>Ad Unit Not Implemented Correctly.</h5>";
        }

        // Get the ad unit
        $adUnit = AdUnit::where('id', $request->input('ad-unit'))->first();

        if (!$adUnit) {
            return "<h5>Ad Unit Not Implemented Correctly.</h5>";
        }


        // Create the ad object
        $ad = new \stdClass();
        $ad->type = 'text';

        $textAd = TextAd::all()->random();

        $imageAd = ImageAdImage::where('size', $adUnit->size)->get()->random();

        $imageAd->imageAd;
        $imageAd->root_url = env('APP_URL');
        $imageAd->image_url = env('APP_URL') . '/' . str_replace("public", "storage", $imageAd->image_location);
        $imageAd->destination_url = $imageAd->imageAd->destination_url;

        if ($imageAd && $textAd) {
            $ad->type = rand(1, 2) == 1 ? 'text' : 'image';
        } else if ($imageAd) {
            $ad->type = 'image';
        } else {
            $ad->type = 'text';
        }


        $ad->textAd = $textAd;
        $ad->imageAd = $imageAd;

        // Get the view that is going to be displayed based on the ad size
        $adPage = null;

        switch ($adUnit->size) {
            case '320X50':
                $adPage = view('ads.textAds.mobile-banner');
                break;
            case '320X100':
                $adPage = view('ads.textAds.large-mobile-banner');
                break;
            case '728X90':
                $adPage = view('ads.textAds.leader-board');
                break;
            case '336X280':
                $adPage = view('ads.textAds.large-rectangle');
                break;
            case '300X250':
                $adPage = view('ads.textAds.medium-rectangle');
                break;
            case '300X600':
                $adPage = view('ads.textAds.sky-scraper');
                break;
            default:
                $adPage = view('ads.textAds.large-rectangle');
        }

        // Save the ad view
        $adView = new AdView();
        if ($ad->type == 'text') {
            $adView->ad_id = $textAd->ad->id;
        } else {
            $adView->ad_id = $imageAd->imageAd->ad->id;
        }
        $adView->content_id = $adUnit->content->id;
        $adView->ad_unit_id = $adUnit->id;
        $adView->save();

        $ad->click_url = env('APP_URL') . '/ads/click?ad-unit=' . $adUnit->id . '&ad=' . $adView->ad_id;

        // Calculate cost and earning
        $publisher = $adUnit->content->publisher;
        $publisher->earnings = $publisher->earnings + 0.5;
        $publisher->save();

        $advertiser = $ad->type == 'text' ? $textAd->ad->advertiser : $imageAd->imageAd->ad->advertiser;
        $advertiser->balance = $advertiser->balance - 0.5;
        $advertiser->save();

        return $adPage->with('ad', $ad);
    }

    public function clickAd(Request $request) {
        // Validate required attributes
        if (!$request->input('ad-unit') || !$request->input('ad')) {
            return "Ad Unit not implemented correctly.";
        }

        $adUnit = AdUnit::findOrFail($request->input('ad-unit'));
        $ad = Ad::findOrFail($request->input('ad'));

        // Create the ad click
        $adClick = new AdClick();
        $adClick->ad_id = $ad->id;
        $adClick->content_id = $adUnit->content->id;
        $adClick->ad_unit_id = $adUnit->id;
        $adClick->save();

        // Calculate cost and earning
        $publisher = $adUnit->content->publisher;
        $publisher->earnings = $publisher->earnings + 1.5;
        $publisher->save();

        $advertiser = $ad->advertiser;
        $advertiser->balance = $advertiser->balance - 1.5;
        $advertiser->save();


        // Redirect to the destination url
        $destinationUrl = $ad->type == 'text' ? $ad->textAd->destination_url : $ad->imageAd->destination_url;
        return redirect($destinationUrl);
    }
}
