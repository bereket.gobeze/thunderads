<?php

namespace App\Http\Controllers;

use App\AdUnit;
use App\AdView;
use App\ImageAdImage;
use App\TelegramChannel;
use App\TextAd;
use GuzzleHttp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;

class TelegramBotController extends Controller
{
    //
    public function webHook(Request $request) {
        $post = json_decode($request->getContent());

        if (isset($post->message)) {
            $this->sendMessage($post->message->from->id, 'erasek ' . $post->message->text . ' nek.');
        } else if (isset($post->channel_post)) {
            $this->handleChannelPost($post);
        }
        return response($post->update_id, 200);
    }

    private function sendMessage($chatId, $message) {
        $url = 'https://api.telegram.org/bot863351576:AAHzlJxJgOD70gNlsFCl9-m6SbIDN6-SjiE/sendMessage?parse_mode=HTML&chat_id=' . $chatId . '&text=' . $message;
        // Make a request to Laravel Passport
        $client = new GuzzleHttp\Client(
            [
                'defaults' => [
                    'verify' => 'false',
                ],
                'curl' => [
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false,
                ]
            ]
        );


        try {
            $client->get($url);
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }
    }

    private function handleChannelPost($post) {
        // check if this message is a channel authentication message
        if ($this->checkChannelAuthenticationMessage($post)) {
            $this->authenticateChannel($post);
            //$this->sendMessage($post->channel_post->chat->id, "Authenticating Channel...");
            return;
        }

        if ($this->isFromRegisteredChannel($post)) {
            $this->postAd($post);
        }
    }

    private function checkChannelAuthenticationMessage($post) {
        if (isset($post->channel_post) & isset($post->channel_post->text)) {
            if ((strpos($post->channel_post->text, 'q1x8jrellhw8')) !== false && (strpos($post->channel_post->text, 'w34xsce29lv') !== false)) {
                return true;
            }
        }

        return false;
    }

    private function isFromRegisteredChannel($post) {
        if (!isset($post->channel_post) || !isset($post->channel_post->chat)) {
            return false;
        }

        // Search for the channel in the db
        $count = TelegramChannel::where('channel_id', $post->channel_post->chat->id)->count();

        return $count > 0;
    }

    private function authenticateChannel($post) {
        $text = $post->channel_post->text;
        $text = str_replace('q1x8jrellhw8', '', $text);
        $text = str_replace('w34xsce29lv', '', $text);

        $adUnitId = intval($text);

        // Find the adUnit
        $adUnit = AdUnit::find($adUnitId);
        $telegramChannel = $adUnit->telegramChannel;

        $telegramChannel->channel_id = $post->channel_post->chat->id;
        $telegramChannel->channel_name = $post->channel_post->chat->title;
        $telegramChannel->save();

        // Delete the authenticating message from the channel
        $this->deleteMessage($post->channel_post->chat->id, $post->channel_post->message_id);
    }

    private function deleteMessage($chat_id, $message_id) {
        $url = 'https://api.telegram.org/bot863351576:AAHzlJxJgOD70gNlsFCl9-m6SbIDN6-SjiE/deleteMessage?chat_id=' . $chat_id . '&message_id=' . $message_id;
        // Make a request to Laravel Passport
        $client = new GuzzleHttp\Client(
            [
                'defaults' => [
                    'verify' => 'false',
                ],
                'curl' => [
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false,
                ]
            ]
        );


        try {
            $client->get($url);
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }
    }

    private function postAd($post) {
        // Get the telegram channel
        $telegramChannel = TelegramChannel::where('channel_id', $post->channel_post->chat->id)->first();

        // Get the ad unit
        $adUnit = $telegramChannel->adUnit;

        $ad = $this->getAd($adUnit);

        if ($ad->type == 'text') {
            $adText = '<b>' . $ad->textAd->headline . '</b>' .
                '%0A%0A' .
                $ad->textAd->ad_text_line_one . '%0A' .
                $ad->textAd->ad_text_line_two . '%0A%0A' .
                '<a href="' . urlencode($ad->click_url) . '">' . $ad->textAd->action_text . '</a>';

            $this->sendMessage($post->channel_post->chat->id, $adText);
        } else if ($ad->type == 'image') {
            //$this->sendMessage($post->channel_post->chat->id, $ad->imageAd->image_url);
            $defaultUrl = urlencode('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQFSFSOyvOK4mc5ZflrnVS0GEBXzZDtAIhZPPCzTH6vYjLYPO3z');

            $url = env('APP_ENV') == 'local' ? $defaultUrl : $ad->imageAd->image_url;

            $caption = '<a href="' . urlencode($ad->click_url) . '">Visit</a>';
            $this->sendPhoto($post->channel_post->chat->id, $url, $caption);
        }
    }

    private function getAd($adUnit) {
        // Create the ad object
        $ad = new \stdClass();
        $ad->type = 'text';

        $textAd = TextAd::all()->random();

        $imageAd = ImageAdImage::all()->random();

        $imageAd->imageAd;
        $imageAd->root_url = env('APP_URL');
        $imageAd->image_url = env('APP_URL') . '/' . str_replace("public", "storage", $imageAd->image_location);
        $imageAd->destination_url = $imageAd->imageAd->destination_url;

        if ($imageAd && $textAd) {
            $ad->type = rand(1, 2) == 1 ? 'text' : 'image';
        } else if ($imageAd) {
            $ad->type = 'image';
        } else {
            $ad->type = 'text';
        }


        $ad->textAd = $textAd;
        $ad->imageAd = $imageAd;


        // Save the ad view
        $adView = new AdView();
        if ($ad->type == 'text') {
            $adView->ad_id = $textAd->ad->id;
        } else {
            $adView->ad_id = $imageAd->imageAd->ad->id;
        }
        $adView->content_id = $adUnit->content->id;
        $adView->ad_unit_id = $adUnit->id;
        $adView->save();

        $ad->click_url = env('APP_URL') . '/ads/click?ad-unit=' . $adUnit->id . '&ad=' . $adView->ad_id;

        return $ad;
    }

    private function sendPhoto($chatId, $photoUrl, $caption = '') {
        $url = 'https://api.telegram.org/bot863351576:AAHzlJxJgOD70gNlsFCl9-m6SbIDN6-SjiE/sendPhoto?parse_mode=HTML&chat_id=' . $chatId . '&photo=' . $photoUrl . '&caption=' . $caption;
        // Make a request to Laravel Passport
        $client = new GuzzleHttp\Client(
            [
                'defaults' => [
                    'verify' => 'false',
                ],
                'curl' => [
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false,
                ]
            ]
        );


        try {
            $client->get($url);
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }
    }

}
