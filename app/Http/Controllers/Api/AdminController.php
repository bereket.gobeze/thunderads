<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\WithdrawRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function acceptWithdrawRequest(WithdrawRequest $withdrawRequest) {
        // Check that the withdraw request is pending
        if ($withdrawRequest->status != 'pending') {
            return response([
                'status' => 'fail',
                'message' => 'Withdraw request already processed.'
            ], 420);
        }

        // Check that the publisher has enough balance to withdraw
        // Get the publisher
        $publisher = $withdrawRequest->publisher;

        if ($publisher->earnings < $withdrawRequest->amount) {
            // Update the status of the request
            $withdrawRequest->status = 'rejected';
            $withdrawRequest->save();

            return response([
                'status' => 'fail',
                'message' => 'Publisher doesn\'t have enough balance.'
            ], 420);
        }

        // Accept the withdraw request
        $withdrawRequest->status = 'completed';
        $withdrawRequest->save();

        // Update publishers balance
        $publisher->earnings = $publisher->earnings - $withdrawRequest->amount;
        $publisher->save();

        return response([
            'status' => 'success',
            'message' => 'Withdraw Completed'
        ]);
    }

    public function rejectWithdrawRequest(WithdrawRequest $withdrawRequest) {
        // Check that the withdraw request is pending
        if ($withdrawRequest->status != 'pending') {
            return response([
                'status' => 'fail',
                'message' => 'Withdraw request already processed.'
            ], 420);
        }

        // Accept the withdraw request
        $withdrawRequest->status = 'rejected';
        $withdrawRequest->save();

        return response([
            'status' => 'success',
            'message' => 'Withdraw Request Rejected'
        ]);
    }

    public function getPendingWithdrawRequests() {
        $requests = WithdrawRequest::where('status', 'pending')->get();

        $finalRequests = [];
        foreach ($requests as $request) {
            $request->publisher->user;
            $finalRequests[] = $request;
        }

        return $finalRequests;
    }

    public function getDashboardStats() {
        // Get the number pending withdraw requests
        $pendingWithdrawRequests = WithdrawRequest::where('status', 'pending')->get()->count();

        return [
            'pending_withdraw_requests' => $pendingWithdrawRequests
        ];
    }

    public function depositToAdvertiser(Request $request) {
        // Validate the inputs
        $request->validate([
            'email' => 'required|string',
            'amount' => 'required|numeric',
        ]);

        // Find the advertiser to be deposited
        $advertiser = User::where('email', $request->input('email'))->firstOrFail()->advertiser;

        // Deposit the amount to the advertiser
        $advertiser->balance = $advertiser->balance + $request->input('amount');
        $advertiser->save();


        return response([
            'status' => 'success',
            'message' => $request->input('amount') . ' deposited to advertiser'
        ]);
    }
}
