<?php

namespace App\Http\Controllers\Api;

use App\Ad;
use App\AdClick;
use App\Advertiser;
use App\AdView;
use App\ImageAd;
use App\ImageAdImage;
use App\TextAd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AdvertiserController extends Controller
{
    //
    public function createAd(Request $request) {
        // Validate the inputs
        $request->validate([
            'name' => 'required|string',
            'gender' => 'required|string',
            'age_range_min' => 'required|numeric',
            'age_range_max' => 'required|numeric',
            'type' => 'required|string',
        ]);

        // Validate text ad properties
        if ($request->input('type') == 'text') {
            $request->validate([
                'destination_url' => 'required|string',
                'headline' => 'required|string',
                'ad_text_line_one' => 'required|string',
                'ad_text_line_two' => 'required|string',
                'action_text' => 'required|string',
            ]);
        } else if ($request->input('type') == 'image') {
            $request->validate([
                'destination_url' => 'required|string',
            ]);
        } else {
            return response([
                'message' => "The given data was invalid",
                'errors' => [
                    'type' => [
                        'Invalid type selected.'
                    ]
                ]
            ], 422);
        }

        // Create the ad
        $ad = new Ad();
        $ad->name = $request->input('name');
        $ad->gender = $request->input('gender');
        $ad->age_range_min = $request->input('age_range_min');
        $ad->age_range_max = $request->input('age_range_max');
        $ad->type = $request->input('type');
        $ad->advertiser_id = $request->user()->advertiser->id;
        $ad->save();

        // Create the text of image ad based on the type parameter
        if ($request->input('type') == 'text') {
            // Create the text ad
            $textAd = new TextAd();
            $textAd->ad_id = $ad->id;
            $textAd->destination_url = $request->input('destination_url');
            $textAd->headline = $request->input('headline');
            $textAd->ad_text_line_one = $request->input('ad_text_line_one');
            $textAd->ad_text_line_two = $request->input('ad_text_line_two');
            $textAd->action_text = $request->input('action_text');
            $textAd->save();
        } else if ($request->input('type') == 'image') {
            // Create the image ad
            $imageAd = new ImageAd();
            $imageAd->ad_id = $ad->id;
            $imageAd->destination_url = $request->input('destination_url');
            $imageAd->save();

            // Upload the images and add them to the db
            if ($request->file('image_one')) {
                $path = $request->file('image_one')->store('public/ads/images');
                $imageAdImage = new ImageAdImage();
                $imageAdImage->image_location = $path;
                $imageAdImage->size = '320X50';
                $imageAdImage->image_ad_id = $imageAd->id;
                $imageAdImage->save();
            }

            if ($request->file('image_two')) {
                $path = $request->file('image_two')->store('public/ads/images');
                $imageAdImage = new ImageAdImage();
                $imageAdImage->image_location = $path;
                $imageAdImage->size = '320X100';
                $imageAdImage->image_ad_id = $imageAd->id;
                $imageAdImage->save();
            }

            if ($request->file('image_three')) {
                $path = $request->file('image_three')->store('public/ads/images');
                $imageAdImage = new ImageAdImage();
                $imageAdImage->image_location = $path;
                $imageAdImage->size = '728X90';
                $imageAdImage->image_ad_id = $imageAd->id;
                $imageAdImage->save();
            }

            if ($request->file('image_four')) {
                $path = $request->file('image_four')->store('public/ads/images');
                $imageAdImage = new ImageAdImage();
                $imageAdImage->image_location = $path;
                $imageAdImage->size = '336X280';
                $imageAdImage->image_ad_id = $imageAd->id;
                $imageAdImage->save();
            }

            if ($request->file('image_five')) {
                $path = $request->file('image_five')->store('public/ads/images');
                $imageAdImage = new ImageAdImage();
                $imageAdImage->image_location = $path;
                $imageAdImage->size = '300X250';
                $imageAdImage->image_ad_id = $imageAd->id;
                $imageAdImage->save();
            }

            if ($request->file('image_six')) {
                $path = $request->file('image_six')->store('public/ads/images');
                $imageAdImage = new ImageAdImage();
                $imageAdImage->image_location = $path;
                $imageAdImage->size = '300X600';
                $imageAdImage->image_ad_id = $imageAd->id;
                $imageAdImage->save();
            }
        }

        return response([
            'status' => 'success',
            'ad' => $ad
        ]);
    }

    public function getAds(Request $request) {
        return Ad::where('advertiser_id', $request->user()->advertiser->id)->get();
    }

    public function getAd(Request $request, Ad $ad) {
        if ($ad->advertiser_id == $request->user()->advertiser->id) {
            // Attach numbers to the ad
            // Get the number of views
            $impressions = AdView::where('ad_id', $ad->id)->get()->count();

            // Get the number of counts
            $clickCount = AdClick::where('ad_id', $ad->id)->get()->count();

            // Get the views
            $views = AdView::where('ad_id', $ad->id)->with(['content'])->orderBy('created_at', 'desc')->limit(6)->get();
            $clicks = AdClick::where('ad_id', $ad->id)->with(['content'])->orderBy('created_at', 'desc')->limit(6)->get();

            // Add them to the ad object
            $ad->view_count = $impressions;
            $ad->click_count = $clickCount;
            $ad->views = $views;
            $ad->clicks = $clicks;

            $ad->textAd;
            $ad->imageAd;

            return $ad;
        } else {
            return response(json_encode(['message' => 'Current advertiser is not the owner of the ad requested.']), 403);
        }
    }

    public function editAd(Request $request, Ad $ad) {
        // Validate the inputs
        $request->validate([
            'name' => 'required|string',
            'gender' => 'required|string',
            'age_range_min' => 'required|numeric',
            'age_range_max' => 'required|numeric',
        ]);

        // Validate and Update the data based on ad type
        if ($ad->type == 'text') {
            $request->validate([
                'destination_url' => 'required|string',
                'headline' => 'required|string',
                'ad_text_line_one' => 'required|string',
                'ad_text_line_two' => 'required|string',
                'action_text' => 'required|string',
            ]);

            // Update the text ad
            $textAd = $ad->textAd;
            $textAd->destination_url = $request->input('destination_url');
            $textAd->headline = $request->input('headline');
            $textAd->ad_text_line_one = $request->input('ad_text_line_one');
            $textAd->ad_text_line_two = $request->input('ad_text_line_two');
            $textAd->action_text = $request->input('action_text');
            $textAd->save();
        } else if ($ad->type == 'image') {
            $request->validate([
                'destination_url' => 'required|string',
            ]);
        }

        // Update the ad
        $ad->name = $request->input('name');
        $ad->gender = $request->input('gender');
        $ad->age_range_min = $request->input('age_range_min');
        $ad->age_range_max = $request->input('age_range_max');
        $ad->save();

        return [
            'status' => 'success'
        ];
    }

    public function getRemainingBalance(Request $request) {
        // Get the logged in advertiser
        $advertiser = $request->user()->advertiser;

        return [
            'balance' => $advertiser->balance
        ];
    }

    public function home(Request $request) {
        // Get the logged in Advertiser
        $advertiser = $request->user()->advertiser;

        // Calculate aggregate stats
        $totalViews = $advertiser->adViews->count();
        $totalClicks = $advertiser->adClicks->count();

        $topClickedAds = $this->getTopClickedAds($advertiser);
        $topViewedAds = $this->getTopViewedAds($advertiser);

        return [
            'clicks' => $totalClicks,
            'views' => $totalViews,
            'most_clicked' => $topClickedAds,
            'most_viewed' => $topViewedAds,
            'advertiser_name' => $advertiser->user->name
        ];

    }

    private function getTopClickedAds(Advertiser $advertiser) {
        $topClickedAdsDbResult = DB::table('ads')
            ->leftJoin('ad_clicks', 'ads.id', '=', 'ad_clicks.ad_id')
            ->selectRaw('count(*) AS total, ad_id')
            ->where('advertiser_id', $advertiser->id)
            ->whereNotNull('ad_id')
            ->groupBy('ad_id')
            ->orderByDesc('total')
            ->limit(3)
            ->get();

        $topClickedAds = [];
        foreach ($topClickedAdsDbResult as $result) {
            $topClickedAds[] = [
                'clicks' => $result->total,
                'ad' => Ad::find($result->ad_id)
            ];
        }

        return $topClickedAds;
    }

    private function getTopViewedAds(Advertiser $advertiser) {
        $topClickedAdsDbResult = DB::table('ads')
            ->leftJoin('ad_views', 'ads.id', '=', 'ad_views.ad_id')
            ->selectRaw('count(*) AS total, ad_id')
            ->where('advertiser_id', $advertiser->id)
            ->whereNotNull('ad_id')
            ->groupBy('ad_id')
            ->orderByDesc('total')
            ->limit(3)
            ->get();

        $topClickedAds = [];
        foreach ($topClickedAdsDbResult as $result) {
            $topClickedAds[] = [
                'views' => $result->total,
                'ad' => Ad::find($result->ad_id)
            ];
        }

        return $topClickedAds;
    }
}
