<?php

namespace App\Http\Controllers\Api;

use App\AdClick;
use App\AdUnit;
use App\AdView;
use App\Content;
use App\ContentTargeting;
use App\Publisher;
use App\TelegramChannel;
use App\WithdrawRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PublisherController extends Controller
{
    //
    public function createContent(Request $request) {
        $request->validate([
            'name' => 'required|string',
            'type' => 'required|string',
            'location' => 'required|string',
            'gender' => 'required|string',
            'category' => 'required|numeric',
            'age_range_max' => 'required|numeric',
            'age_range_min' => 'required|numeric',
        ]);

        // Create the content
        $content = new Content();
        $content->name = $request->input('name');
        $content->type = $request->input('type');
        $content->location = $request->input('location');
        $content->publisher_id = $request->user()->publisher->id;
        $content->save();

        // Create the content targeting
        $contentTargeting = new ContentTargeting();
        $contentTargeting->gender = $request->input('gender');
        $contentTargeting->content_id = $content->id;
        $contentTargeting->content_category_id = $request->input('category');
        $contentTargeting->age_range_max = $request->input('age_range_max');
        $contentTargeting->age_range_min = $request->input('age_range_min');
        $contentTargeting = $contentTargeting->save();

        // If content is a telegram, create the ad unit here
        if ($content->type == 'telegram') {
            $adUnit = new AdUnit();
            $adUnit->name = 'Channel Ad Unit';
            $adUnit->type = 'both';
            $adUnit->content_id = $content->id;
            $adUnit->size = 'telegram';
            $adUnit->save();

            // Create a telegram channel object
            $telegramChannel = new TelegramChannel();
            $telegramChannel->ad_unit_id = $adUnit->id;
            $telegramChannel->save();
        }

        return json_encode(['status' => 'success']);
    }

    public function getContent(Request $request, Content $content) {
        if ($request->user()->publisher->id == $content->publisher->id) {
            $contentCategory = $content->contentTargeting->contentCategory;

            $content->views = AdView::where('content_id', $content->id)->count();
            $content->clicks = AdClick::where('content_id', $content->id)->count();

            return $content;
        } else {
            return response(json_encode(['message' => 'Current publisher is not the owner of the content requested.']), 403);
        }
    }

    public function getContents(Request $request) {
        $contents =  Content::where('publisher_id', $request->user()->publisher->id)->get();

        // Loop through all the contents and call their adUnits method to fetch the associated ad units
        foreach ($contents as $content) {
            $content->adUnits;
        }

        return $contents;
    }

    public function editContent(Request $request, Content $content) {
        $request->validate([
            'name' => 'required|string',
            'location' => 'required|string',
            'gender' => 'required|string',
            'category' => 'required|numeric',
            'age_range_max' => 'required|numeric',
            'age_range_min' => 'required|numeric',
        ]);

        // Fetch the content and its related content targeting to be modified
        $content = Content::findOrFail($content->id);
        $contentTargeting = $content->contentTargeting;

        // Update the content with the new data
        $content->name = $request->input('name');
        $content->location = $request->input('location');

        $contentTargeting->gender = $request->input('gender');
        $contentTargeting->age_range_min = $request->input('age_range_min');
        $contentTargeting->age_range_max = $request->input('age_range_max');
        $contentTargeting->content_category_id = $request->input('category');

        // Save the changes
        $content->save();
        $contentTargeting->save();

        return [
            'status' => 'success',
            'content' => $content
        ];
    }

    public function createAdUnit(Request $request, Content $content) {
        // Validate the inputs
        $request->validate([
            'name' => 'required|string',
            'size' => 'required|string',
        ]);

        // Check that the content passed is owned by the logged in user
        if ($content->publisher->id != $request->user()->publisher->id) {
            return response(['message' => 'You do not own this content'], 403);
        }

        // Create the new Ad Unit
        $adUnit = new AdUnit();
        $adUnit->name = $request->input('name');
        $adUnit->type = 'both';
        $adUnit->content_id = $content->id;
        $adUnit->size = $request->input('size');
        $adUnit->save();

        return json_encode(['status' => 'success', 'adUnit' => $adUnit]);
    }

    public function getAdUnits(Request $request, Content $content) {
        if ($content->publisher->id == $request->user()->publisher->id) {
            return AdUnit::where('content_id', $content->id)->get();
        } else {
            return response(['message' => 'You do not own this content.'], 403);
        }
    }

    public function getAdUnit(Request $request, Content $content, AdUnit $adUnit) {
        // Check that the content passed is owned by the logged in user
        if ($content->publisher->id != $request->user()->publisher->id) {
            return response(['message' => 'You do not own this content'], 403);
        }

        $adUnit->content;
        $adUnit->root_url = env('APP_URL');
        if ($adUnit->content->type == 'telegram') {
            $adUnit->telegramChannel;
        }

        // Include View and Click numbers with the adUnit
        $adUnit->views = AdView::where('ad_unit_id', $adUnit->id)->count();
        $adUnit->clicks = AdClick::where('ad_unit_id', $adUnit->id)->count();

        return $adUnit;
    }

    public function editAdUint(Request $request, Content $content, AdUnit $adUnit) {
        // Validate the inputs
        $request->validate([
            'name' => 'required|string',
            'size' => 'required|string',
        ]);

        // Set the modified attributes
        $adUnit->name = $request->input('name');
        $adUnit->size = $request->input('size');

        // Save the changes
        $adUnit->save();

        return [
            'status' => 'success'
        ];
    }

    public function createWithdrawRequest(Request $request) {
        $request->validate([
            'amount' => 'required|numeric',
        ]);

        // Get the logged in publisher
        $publisher = $request->user()->publisher;

        // Check if publisher has enough balance
        if ($request->input('amount') > $publisher->earnings) {
            return response([
                'message' => "The given data was invalid",
                'errors' => [
                    'amount' => [
                        'You don\'t have enough balance.'
                    ]
                ]
            ], 422);
        }

        // Create the withdraw request
        $withdrawRequest = new WithdrawRequest();
        $withdrawRequest->amount = $request->input('amount');
        $withdrawRequest->publisher_id = $publisher->id;
        $withdrawRequest->status = 'pending';
        $withdrawRequest->save();

        return response([
            'status' => 'success'
        ]);
    }

    public function walletDetails(Request $request) {
        // Get the logged in publisher
        $publisher = $request->user()->publisher;

        // Get the withdraw requests by the publisher
        $withdrawRequests = WithdrawRequest::where('publisher_id', $publisher->id)->get();

        return [
            'balance' => $publisher->earnings,
            'withdraw_requests' => $withdrawRequests
        ];
    }

    public function home(Request $request) {
        // Get the publisher
        $publisher = $request->user()->publisher;

        // Get the stats
        $totalViews = $publisher->adViews->count();
        $totalClicks = $publisher->adClicks->count();

        $topClickedContent = $this->getTopClickedContents($publisher);
        $topViewedContent = $this->getTopViewedContents($publisher);

        return [
            'clicks' => $totalClicks,
            'views' => $totalViews,
            'most_clicked' => $topClickedContent,
            'most_viewed' => $topViewedContent,
            'publisher_name' => $publisher->user->name
        ];
    }

    private function getTopClickedContents(Publisher $publisher) {
        $topClickedContentsDbResult = DB::table('contents')
            ->leftJoin('ad_clicks', 'contents.id', '=', 'ad_clicks.content_id')
            ->selectRaw('count(*) AS total, content_id')
            ->where('publisher_id', $publisher->id)
            ->whereNotNull('content_id')
            ->groupBy('content_id')
            ->orderByDesc('total')
            ->limit(3)
            ->get();

        $topClickedContents = [];
        foreach ($topClickedContentsDbResult as $result) {
            $topClickedContents[] = [
                'clicks' => $result->total,
                'content' => Content::find($result->content_id)
            ];
        }

        return $topClickedContents;
    }

    private function getTopViewedContents(Publisher $publisher) {
        $topClickedContentsDbResult = DB::table('contents')
            ->leftJoin('ad_views', 'contents.id', '=', 'ad_views.content_id')
            ->selectRaw('count(*) AS total, content_id')
            ->where('publisher_id', $publisher->id)
            ->whereNotNull('content_id')
            ->groupBy('content_id')
            ->orderByDesc('total')
            ->limit(3)
            ->get();

        $topClickedContents = [];
        foreach ($topClickedContentsDbResult as $result) {
            $topClickedContents[] = [
                'views' => $result->total,
                'content' => Content::find($result->content_id)
            ];
        }

        return $topClickedContents;
    }
}
