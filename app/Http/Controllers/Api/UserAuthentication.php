<?php

namespace App\Http\Controllers\Api;

use App\User;
use GuzzleHttp;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserAuthentication extends Controller
{
    public function login(Request $request) {
        // Validate the inputs
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);


        // Get the inputs
        $email = $request->input('email');
        $password = $request->input('password');


        // Make a request to Laravel Passport
        $http = new GuzzleHttp\Client();

        try {
            $response = $http->post(env('APP_URL') . '/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => env('passport_password_grant_client_id'),
                    'client_secret' => env('passport_password_grant_client_secret'),
                    'username' => $email,
                    'password' => $password,
                    'scope' => 'publisher',
                ],
            ]);
        } catch (ClientException $exception) {
            return $exception->getResponse();
        }

        $response = json_decode((string) $response->getBody(), true);
        $response['user_type'] = User::where('email', $email)->first()->user_type;
        return $response;
    }

    public function userType(Request $request) {
        return $request->user()->user_type;
    }

    public function changePassword(Request $request) {
        $request->validate([
            'password' => 'required|string',
            'new_password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if (Hash::check($request->input('password'), $request->user()->password)) {
            $user = $request->user();
            $user->password = Hash::make($request->input('new_password'));
            $user->save();

            return response('Success');
        }

        return response('Password incorrect', 401);

    }

    public function getUserInfo(Request $request) {
        // Get the logged in user
        return $request->user();
    }
}
