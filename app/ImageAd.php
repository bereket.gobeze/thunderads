<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageAd extends Model
{
    //
    protected $fillable = [
        'ad_id', 'destination_url'
    ];

    public function ad() {
        return $this->belongsTo('App\Ad');
    }

    public function images() {
        return $this->hasMany('App\ImageAdImage');
    }
}
