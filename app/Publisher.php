<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    //
    protected $fillable = [
        'earnings', 'user_id'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function content() {
        return $this->hasMany('App\Content');
    }

    public function withdrawRequests() {
        return $this->hasMany('App\WithdrawRequest');
    }

    public function adClicks() {
        return $this->hasManyThrough('App\AdClick', 'App\Content');
    }

    public function adViews() {
        return $this->hasManyThrough('App\AdView', 'App\Content');
    }
}
