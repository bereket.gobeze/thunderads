@extends('layouts.app')

@section('content')

    <section>

        <span style="float: right; position: relative"><a href="#" class="font-weight-bold h5 mb-0"><i class="far fa-edit"></i> EDIT</a> </span>

        <h2 class="pb-2 mb-2">ASB Promo</h2>

        <div class="card p-3 bg-transparent">
            <h3 class="card-title">Summary</h3>
            <div class="card-body row">
                <div class="col-3">
                    <span class="h3">1024</span>
                    <br>Impressions
                </div>

                <div class="col-3">
                    <span class="h3">Br 189.00</span>
                    <br>Cost
                </div>

                <div class="col-3">
                    <span class="h3">28</span>
                    <br>Clicks
                </div>

                <div class="col-3">
                    <span class="h3">29%</span>
                    <br>CTR
                </div>
            </div>
        </div>

        <hr>

        <h3 class="pb-3 mb-3">Performance</h3>


        <div class="row mx-auto">
            <div class="col-12 col-md-6 col-lg-4">
                <div class="card" style="min-height: 12rem;">
                    <div class="card-body">
                        <h4 class="card-title">Some Metric</h4>

                        <div class="w-100 bg-warning text-center" style="height: 6rem;">
                            <span>The Graph</span>
                        </div>
                    </div>

                    <div class="card-footer bg-transparent">
                        <h4 class="mb-0 text-right font-weight-bold">1000</h4>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="card" style="min-height: 12rem;">
                    <div class="card-body">
                        <h4 class="card-title">Another Metric</h4>

                        <div class="w-100 bg-danger text-center" style="height: 6rem;">
                            <span>The Graph</span>
                        </div>
                    </div>

                    <div class="card-footer bg-transparent">
                        <h4 class="mb-0 text-right font-weight-bold">36%</h4>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="card" style="min-height: 12rem;">
                    <div class="card-body">
                        <h4 class="card-title">That Metric</h4>

                        <div class="w-100 bg-success text-center" style="height: 6rem;">
                            <span>The Graph</span>
                        </div>
                    </div>

                    <div class="card-footer bg-transparent">
                        <h4 class="mb-0 text-right font-weight-bold">Br 26.34</h4>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="card" style="min-height: 12rem;">
                    <div class="card-body">
                        <h4 class="card-title">Some Metric</h4>

                        <div class="w-100 bg-warning text-center" style="height: 6rem;">
                            <span>The Graph</span>
                        </div>
                    </div>

                    <div class="card-footer bg-transparent">
                        <h4 class="mb-0 text-right font-weight-bold">1000</h4>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="card" style="min-height: 12rem;">
                    <div class="card-body">
                        <h4 class="card-title">Some Metric</h4>

                        <div class="w-100 bg-warning text-center" style="height: 6rem;">
                            <span>The Graph</span>
                        </div>
                    </div>

                    <div class="card-footer bg-transparent">
                        <h4 class="mb-0 text-right font-weight-bold">1000</h4>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="card" style="min-height: 12rem;">
                    <div class="card-body">
                        <h4 class="card-title">Some Metric</h4>

                        <div class="w-100 bg-warning text-center" style="height: 6rem;">
                            <span>The Graph</span>
                        </div>
                    </div>

                    <div class="card-footer bg-transparent">
                        <h4 class="mb-0 text-right font-weight-bold">1000</h4>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="card" style="min-height: 12rem;">
                    <div class="card-body">
                        <h4 class="card-title">Some Metric</h4>

                        <div class="w-100 bg-warning text-center" style="height: 6rem;">
                            <span>The Graph</span>
                        </div>
                    </div>

                    <div class="card-footer bg-transparent">
                        <h4 class="mb-0 text-right font-weight-bold">1000</h4>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="card" style="min-height: 12rem;">
                    <div class="card-body">
                        <h4 class="card-title">Some Metric</h4>

                        <div class="w-100 bg-warning text-center" style="height: 6rem;">
                            <span>The Graph</span>
                        </div>
                    </div>

                    <div class="card-footer bg-transparent">
                        <h4 class="mb-0 text-right font-weight-bold">1000</h4>
                    </div>
                </div>
            </div>
        </div>



    </section>

@endsection