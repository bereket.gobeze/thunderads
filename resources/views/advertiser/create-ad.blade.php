@extends('layouts.app')

@section('content')
    <section>
        <h2 class="pb-2">Create Ad</h2>

        <form class="pr-lg-5">
            <div class="form-group">
                <label for="campaignNameInput">Campaign Name</label>
                <input type="text" class="form-control" id="campaignNameInput" placeholder="Eg: Gebeta">
            </div>

            <div class="form-group">
                <label for="">Ad Type</label>
                <select class="custom-select" id="adTypeSelect">
                    <option selected>Choose One</option>
                    <option value="text">Text</option>
                    <option value="image">Image</option>
                    <option value="video">Video</option>
                </select>
            </div>

            <hr>

            <h3 class="pb-2">Targeting</h3>

            <div class="px-3">
                <h5>Location</h5>

                <div class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" name="locationOption" id="locationOptionAll" value="all" checked>
                    <label class="custom-control-label" for="locationOptionAll">All Available Locations</label>
                </div>

                <div class="custom-control custom-radio">
                    <input class="custom-control-input" type="radio" name="locationOption" id="locationOptionAdd" value="add">
                    <label class="custom-control-label" for="locationOptionAdd">Add Specific</label>
                </div>

                <div class="px-4 py-1">
                <span class="text-muted">
                    <span class="pr-2 py-1"><i class="fas fa-map-marker-alt"></i></span>
                    Addis Ababa, Ethiopia
                    <span class="float-right">X</span>
                </span>

                    <div class="form-group py-2" style="max-width: 25rem;">
                        <input type="text" class="form-control" id="locationSearch" placeholder="Search Location">
                        <button class="btn btn-primary mt-2 float-right">ADD</button>
                    </div>

                </div>
            </div>


            <h3 class="pb-2">Demography</h3>

            <div class="px-3">
                <h5>Gender</h5>

                <div class="custom-control custom-checkbox custom-control-inline">
                    <input type="checkbox" class="custom-control-input" id="genderCheckboxMale">
                    <label class="custom-control-label" for="genderCheckboxMale">Male</label>
                </div>

                <div class="custom-control custom-checkbox custom-control-inline">
                    <input type="checkbox" class="custom-control-input" id="genderCheckboxFemale">
                    <label class="custom-control-label" for="genderCheckboxFemale">Female</label>
                </div>
            </div>

            <div class="px-3 pt-3">
                <h5>Age</h5>

                <div class="pl-3">
                    <div class="custom-control custom-checkbox custom-control pb-1">
                        <input type="checkbox" class="custom-control-input" id="ageRangeCheckBoxOne">
                        <label class="custom-control-label" for="ageRangeCheckBoxOne">18-24</label>
                    </div>

                    <div class="custom-control custom-checkbox custom-control pb-1">
                        <input type="checkbox" class="custom-control-input" id="ageRangeCheckBoxTwo">
                        <label class="custom-control-label" for="ageRangeCheckBoxTwo">24-30</label>
                    </div>

                    <div class="custom-control custom-checkbox custom-control pb-1">
                        <input type="checkbox" class="custom-control-input" id="ageRangeCheckBoxThree">
                        <label class="custom-control-label" for="ageRangeCheckBoxThree">31-36</label>
                    </div>

                    <div class="custom-control custom-checkbox custom-control pb-1">
                        <input type="checkbox" class="custom-control-input" id="ageRangeCheckBoxFour">
                        <label class="custom-control-label" for="ageRangeCheckBoxFour">37-45</label>
                    </div>

                    <div class="custom-control custom-checkbox custom-control pb-1">
                        <input type="checkbox" class="custom-control-input" id="ageRangeCheckBoxFive">
                        <label class="custom-control-label" for="ageRangeCheckBoxFive">46-65+</label>
                    </div>
                </div>
            </div>



        </form>
    </section>
@endsection