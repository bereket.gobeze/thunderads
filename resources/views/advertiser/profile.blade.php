@extends('layouts.app')

@section('content')
    <section>
        <h2>Profile</h2>

        <form>
            <div class="form-group">
                <label for="nameInput">Name</label>
                <input type="text" class="form-control" id="nameInput" placeholder="Bereket Gobeze">
            </div>

            <div class="form-group">
                <label for="emailInput">Email address</label>
                <input type="email" class="form-control" id="emailInput" aria-describedby="emailHelp" value="bereket.nyb@gmail.com" disabled>
                <small id="emailHelp" class="form-text text-muted">You can not change your email.</small>
            </div>

            <div class="form-group">
                <label for="otherInput">Other Detail</label>
                <input type="text" class="form-control" id="otherInput" placeholder="Something something">
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>

        <hr>

        <h3>Change Password</h3>

        <form>
            <div class="form-group">
                <label for="oldPasswordInput">Old Password</label>
                <input type="password" class="form-control" id="oldPasswordInput" placeholder="Password">
            </div>

            <div class="form-group">
                <label for="newPasswordInput">New Password</label>
                <input type="password" class="form-control" id="newPasswordInput" placeholder="Password">
            </div>

            <div class="form-group">
                <label for="passwordConfirmInput">Confirm Password</label>
                <input type="password" class="form-control" id="passwordConfirmInput" placeholder="Password">
            </div>

            <button type="submit" class="btn btn-primary">CHANGE</button>
        </form>
    </section>
@endsection