@extends('layouts.app')

@section('content')
    <span style="float: right; position: relative"><a href="#" class="font-weight-bold h5 mb-0"><i class="far fa-plus-square"></i> NEW</a> </span>

    <h2>My Ads</h2>

    <br>

    <form class="form-inline">
        <label class="sr-only" for="searchInput">Search</label>
        <input type="text" class="form-control mb-2 mr-sm-2 w-100" id="searchInput" placeholder="Search">

        <button type="submit" class="btn btn-primary mb-2">Search</button>
    </form>

    <div class="row mx-auto">

        <div class="col-md-4 p-1">
            <div class="card bg-transparent " style="min-height: 8rem;">
                <div class="card-body">
                    <span class="card-title h4">ASB Promo</span>
                </div>

                <div class="card-footer bg-transparent">
                    <a href="#" class="my-0 float-right">Manage</a>
                </div>

            </div>
        </div>

        <div class="col-md-4 p-1">
            <div class="card bg-transparent " style="min-height: 8rem;">
                <div class="card-body">
                    <span class="card-title h4">TA Concert</span>
                </div>

                <div class="card-footer bg-transparent">
                    <a href="#" class="my-0 float-right">Manage</a>
                </div>

            </div>
        </div>

        <div class="col-md-4 p-1">
            <div class="card bg-transparent " style="min-height: 8rem;">
                <div class="card-body">
                    <span class="card-title h4">Anbessa Promo</span>
                </div>

                <div class="card-footer bg-transparent">
                    <a href="#" class="my-0 float-right">Manage</a>
                </div>

            </div>
        </div>

        <div class="col-md-4 p-1">
            <div class="card bg-transparent " style="min-height: 8rem;">
                <div class="card-body">
                    <span class="card-title h4">Push</span>
                </div>

                <div class="card-footer bg-transparent">
                    <a href="#" class="my-0 float-right">Manage</a>
                </div>

            </div>
        </div>

        <div class="col-md-4 p-1">
            <div class="card bg-transparent " style="min-height: 8rem;">
                <div class="card-body">
                    <span class="card-title h4">Sample Ad</span>
                </div>

                <div class="card-footer bg-transparent">
                    <a href="#" class="my-0 float-right">Manage</a>
                </div>

            </div>
        </div>
    </div>
@endsection