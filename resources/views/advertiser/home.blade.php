@extends('layouts.app')

@section('content')

    <div class="px-2">

        <h2>Campaign Performance Summary</h2>

        <div class="card bg-primary text-white" style="min-height: 12rem;">
            <div class="card-body">
                This is where the performance layout goes
            </div>
        </div>

    </div>

@endsection