<div class="side-bar">
    <ul style="list-style: none; padding: 0;">
        <li><a href="#" class="side-bar-items"><i class="fas fa-home"></i> Home</a></li>
        <li><a href="#" class="side-bar-items active"><i class="fab fa-android"></i> My Apps</a></li>
        <li><a href="#" class="side-bar-items"><i class="fas fa-wallet"></i> Earnings</a></li>
        <li><a href="#" class="side-bar-items"><i class="fas fa-chart-bar"></i> Performance</a></li>
        <li><a href="#" class="side-bar-items"><i class="fas fa-cog"></i> Settings</a></li>
        <li><a href="#" class="side-bar-items"><i class="fas fa-bomb"></i> Important Thing</a></li>
        <li><a href="#" class="side-bar-items"><i class="fas fa-receipt"></i> Another One</a></li>
        <li><a href="#" class="side-bar-items"><i class="fas fa-info-circle"></i> About</a></li>
    </ul>
</div>