<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <title>Thunder Ads</title>

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #181818;
            font-family: Helvetica, Arial, sans-serif;
            font-weight: 200;
            font-size: 1rem;
            height: 50px;
            width: 320px;
            margin: 0;
        }

        #ad {
            background-color: #fff;
            height: 50px;
            width: 320px;
            border: 1px solid #efefef;
            border-radius: 6px;
        }

        .main {

        }

        .text-container {
            float: left;
            width: 75%;
            padding: 10px;
            display: inline;
        }

        #headline {
            margin: 0;
            color: #428BCA;
        }

        #adTextLineOne {
            margin: 3px 0 0 0;
            display: none;
        }

        #adTextLineTwo {
            margin: 3px 0 0 0;
            display: none;
        }

        #actionText {
            margin: 3px 0 0 0;
            display: none;
            text-decoration: none;
        }

        #actionButton {
            float: right;
            background-color: #428BCA;
            border-radius: 25px;
            width: 45px;
            height: 45px;
            margin: 2px 4px;
            text-align: center;
            box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14),
            0 1px 5px 0 rgba(0, 0, 0, 0.12),
            0 3px 1px -2px rgba(0, 0, 0, 0.2);
        }

        .arrow {
            font-weight: bold;
            color: #fff;
            font-size: 2.2rem;
        }


    </style>
</head>
<body>

    <div id="ad">

    <div class="main">

        @if($ad->type == 'text')
            <div class="text-container">
                <h2 id="headline">{{$ad->textAd->headline}}</h2>

                <p id="adTextLineOne">
                    {{$ad->textAd->ad_text_line_one}}
                </p>

                <p id="adTextLineTwo">
                    {{$ad->textAd->ad_text_line_two}}
                </p>

                <a href="{{$ad->click_url}}" target="_parent" id="actionText">
                    {{$ad->textAd->action_text}}
                </a>
            </div>

            <div id="actionButton">
                <span class="arrow">›</span>
            </div>
        @endif
        @if($ad->type == 'image')
            <a href="{{$ad->click_url}}" target="_parent">
                <img class="ad-image" src="{{$ad->imageAd->image_url}}">
            </a>
        @endif
    </div>

</div>

</body>

<script>
    let current = 2;

    // Get the texts
    let headline = document.getElementById("headline");
    let adTextLineOne = document.getElementById("adTextLineOne");
    let adTextLineTwo = document.getElementById("adTextLineTwo");
    let actionText = document.getElementById("actionText");

    setInterval(changeTexts, 2000);

    function changeTexts() {
        console.log("Change Texts: " + current);
        // Change the visible text based on the current flag
        if (current === 1) {
            headline.style.display = "block";
            adTextLineOne.style.display = "none";
            adTextLineTwo.style.display = "none";
            actionText.style.display = "none";
        } else if (current === 2) {
            headline.style.display = "none";
            adTextLineOne.style.display = "block";
            adTextLineTwo.style.display = "none";
            actionText.style.display = "none";
        } else if (current === 3) {
            headline.style.display = "none";
            adTextLineOne.style.display = "none";
            adTextLineTwo.style.display = "block";
            actionText.style.display = "none";
        } else if (current === 4) {
            headline.style.display = "none";
            adTextLineOne.style.display = "none";
            adTextLineTwo.style.display = "none";
            actionText.style.display = "block";
        }
        current++;
        current = current > 4 ? 0 : current;
    }

</script>

</html>
