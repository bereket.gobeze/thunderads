<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <title>Thunder Ads</title>

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #a7a7a7;
            font-family: Helvetica, Arial, sans-serif;
            font-weight: 200;
            font-size: 1rem;
            height: 90px;
            width: 728px;
            margin: 0;
        }

        a {
            text-decoration: none;
        }

        #ad {
            background-color: #fff;
            height: 90px;
            width: 728px;
            border: 1px solid #efefef;
            border-radius: 6px;
        }

        .main {
            display: inline;
        }

        .text-container {
            float: left;
            width: 70%;
            height: 100%;
            //display: inline;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            align-items: center;
        }

        #headline {
            margin: 0;
            color: #000000;
            padding: 10px;
            width: 100%;
        }

        #adTextLineOne {
            padding: 0 10px;
            margin-top: 0;
        }

        #adTextLineTwo {
            margin: 3px 0 0 0;
        }

        #actionText {
            margin: 3px 0 0 0;
            display: none;
        }

        #actionButtonContainer {
            float: right;
            width: 25%;
            height: 100%;
            text-align: center;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
        }

        #actionButton {
            background-color: #428BCA;
            border-radius: 25px;
            padding: 8px 24px;
            box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14),
            0 1px 5px 0 rgba(0, 0, 0, 0.12),
            0 3px 1px -2px rgba(0, 0, 0, 0.2);
        }

        .arrow {
            color: #fff;
            font-size: 1.2rem;
        }


    </style>
</head>
<body>

<div id="ad">

    <div class="main">


        @if($ad->type == 'text')
            <div class="text-container">
                <h2 id="headline">{{$ad->textAd->headline}}</h2>

                <p id="adTextLineOne">
                    {{$ad->textAd->ad_text_line_one}} {{$ad->textAd->ad_text_line_two}}
                </p>

            </div>

            <div id="actionButtonContainer">
            <div id="actionButton">
                <a href="{{$ad->click_url}}" target="_parent" class="arrow">{{$ad->textAd->action_text}}</a>
            </div>
        </div>
        @endif
        @if($ad->type == 'image')
            <a href="{{$ad->click_url}}" target="_parent">
                <img class="ad-image" src="{{$ad->imageAd->image_url}}">
            </a>
        @endif

    </div>

</div>

</body>

<script>


</script>

</html>
