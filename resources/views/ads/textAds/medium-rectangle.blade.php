<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <title>Thunder Ads</title>

    <style>
        html, body {
            background-color: #fff;
            color: #181818;
            font-family: Helvetica, Arial, sans-serif;
            font-weight: 200;
            font-size: 1rem;
            height: 250px;
            width: 300px;
            margin: 0;
        }

        a {
            text-decoration: none;
        }

        #ad {
            background-color: #fff;
            height: 250px;
            width: 300px;
            border: 1px solid #efefef;
            border-radius: 6px;
        }

        .main {
            height: 100%;
        }

        .top {
            height: 70%;
            width: 100%;
        }

        .bottom {
            height: 30%;
            width: 100%;
            background: #d2d2d2;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            border-bottom-right-radius: 6px;
            border-bottom-left-radius: 6px;
        }

        #headline {
            display: block;
            padding: 20px;
            margin: 0 0 20px;
            font-size: 24px;
            text-align: center;
        }

        #adTextLineOne {
            display: block;
            margin: 2px 8px;
            font-size: 16px;
            color: rgba(0, 0, 0, 0.55);
            text-align: center;
        }

        #adTextLineTwo {
            display: block;
            margin: 2px 8px;
            font-size: 16px;
            color: rgba(0, 0, 0, 0.55);
            text-align: center;
        }

        #actionButton {
            color: #fff;
            background-color: #428BCA;
            padding: 8px 24px;
            border-radius: 25px;
            box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14),
            0 1px 5px 0 rgba(0, 0, 0, 0.12),
            0 3px 1px -2px rgba(0, 0, 0, 0.2);
        }






    </style>
    <!-- Styles -->
</head>
<body>

<div id="ad">

    <div class="main">

        @if($ad->type == 'text')
            <div class="top">
                <h4 id="headline">{{$ad->textAd->headline}}</h4>

                <small id="adTextLineOne">{{$ad->textAd->ad_text_line_one}}</small>

                <small id="adTextLineTwo">{{$ad->textAd->ad_text_line_two}}</small>

            </div>

            <div class="bottom">
                <a href="{{$ad->click_url}}" target="_parent" id="actionButton">{{$ad->textAd->action_text}}</a>
            </div>
        @endif
        @if($ad->type == 'image')
            <a href="{{$ad->click_url}}" target="_parent">
                <img class="ad-image" src="{{$ad->imageAd->image_url}}">
            </a>
        @endif

    </div>

</div>

</body>

<script>


</script>

</html>
