<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <title>Thunder Ads</title>

    <style>
        html, body {
            background-color: #fff;
            color: #181818;
            font-family: Helvetica, Arial, sans-serif;
            font-weight: 200;
            font-size: 1rem;
            height: 100px;
            width: 320px;
            margin: 0;
        }

        #ad {
            background-color: #fff;
            height: 100px;
            width: 320px;
            border: 1px solid #efefef;
            border-radius: 6px;
        }

        .main {
            height: 100%;
        }

        .top {
            height: 60%;
            width: 100%;
        }

        .bottom {
            height: 40%;
            width: 100%;
            background: #d2d2d2;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            border-bottom-right-radius: 6px;
            border-bottom-left-radius: 6px;
        }

        #headline {
            margin: 4px 8px 4px;
            font-size: 15px;
        }

        #adTextLineOne {
            display: block;
            margin: 2px 8px;
            font-size: 12px;
            color: rgba(0, 0, 0, 0.55);
        }

        #adTextLineTwo {
            display: block;
            margin: 2px 8px;
            font-size: 12px;
            color: rgba(0, 0, 0, 0.55);
        }

        #actionButton {
            color: #fff;
            background-color: #428BCA;
            font-weight: bold;
            padding: 4px 16px;
            border-radius: 15px;
            text-decoration: none;
        }


        .ad-image {
            height: 100px;
            width: 320px;
        }





    </style>
    <!-- Styles -->
</head>
<body>

<div id="ad">

    <div class="main">

        @if($ad->type == 'text')

            <div class="top">
                <h4 id="headline">{{$ad->textAd->headline}}</h4>

                <small id="adTextLineOne">{{$ad->textAd->ad_text_line_one}}</small>

                <small id="adTextLineTwo">{{$ad->textAd->ad_text_line_two}}</small>

            </div>

            <div class="bottom">
                <a href="{{$ad->click_url}}" target="_parent" id="actionButton">{{$ad->textAd->action_text}}</a>
            </div>

        @endif

        @if($ad->type == 'image')
            <a href="{{$ad->click_url}}" target="_parent">
                <img class="ad-image" src="{{$ad->imageAd->image_url}}">
            </a>
        @endif

    </div>

</div>

</body>

<script>


</script>

</html>
