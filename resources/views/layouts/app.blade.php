<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:400,700" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


</head>
<body>
    <div id="app">

        @yield('content')
        {{--
        @include('includes.navbar')

        @include('includes.sidebar')

        <main class="py-4 px-3 px-lg-5 main">
            @yield('content')
        </main>
        --}}
    </div>

    <script>
        window.openSideBar = function() {
            if($('.side-bar').width() === 0) {
                document.getElementById("sideBar").style.width = "16rem";
                mobileEventSideBarOpened = true;
            }
            else {
                closeSideBar();
                mobileEventSideBarOpened = false;
            }
        };

        window.toggleMobileEventsSideBar = function() {
            console.log('Got here ' + window.innerWidth);
            if (window.innerWidth < 992) {
                $('.side-bar').width($('.side-bar').width() === 0 ? 250 : 0);
                mobileEventSideBarOpened = $('.side-bar').width() !== 0;
            }
        };

        function closeSideBar() {
            document.getElementById("sideBar").style.width = "0px";
        }

        // Called on resize event of the window.
        // Show and hide the side navigation and the bet slip based on the device width
        let mobileEventSideBarOpened = false;
        window.onresize = function() {
            if (window.innerWidth >= 992) { // For desktop devices and larger
                // Show the side bar
                document.getElementById("sideBar").style.width = "16rem";

                // Set the events side bar opened to false
                mobileEventSideBarOpened = false;
            } else if (window.innerWidth < 992) { // For tablet devices and smaller
                if (!mobileEventSideBarOpened) {
                    // Hide the side bar
                    document.getElementById("sideBar").style.width = "0px";
                }
            }

        };
    </script>
</body>
</html>
