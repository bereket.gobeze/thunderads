@extends('layouts.app')

@section('content')

    <template v-if="this.$route.path != '/login' && this.$route.path != '/register'">
        <nav-bar></nav-bar>

        <side-bar></side-bar>

        <main class="">
            <template v-if="this.$route.path == '/'">
                <router-view></router-view>
            </template>
            <template v-else>
                <router-view class="main py-4 px-3 px-lg-5"></router-view>
            </template>
        </main>
    </template>

    <template v-else>
        <router-view></router-view>
    </template>

@endsection