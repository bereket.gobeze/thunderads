@extends('layouts.app')

@section('content')
    <section>

        <h2>Add a New Content</h2>



        <form>

            <h4>Choose a Platform</h4>

            <div class="row mx-0">

                <div class="col-12 col-md-4 col-lg-3 p-1">
                    <div class="card content-type-card content-type-android">
                        <div class="card-body text-center">
                            <i class="text-center fab fa-android"></i>
                            <span class="content-type-card-label">Android</span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4 col-lg-3 p-1">
                    <div class="card content-type-card content-type-telegram">
                        <div class="card-body text-center">
                            <i class="text-center fab fa-telegram-plane"></i>
                            <span class="content-type-card-label">Telegram</span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4 col-lg-3 p-1">
                    <div class="card content-type-card content-type-web">
                        <div class="card-body text-center">
                            <i class="text-center fas fa-globe"></i>
                            <span class="content-type-card-label">Website</span>
                        </div>
                    </div>
                </div>

            </div>

            <span class="p-3"></span>

            <div class="row">
                <div class="col-12 col-lg-9">

                    <h6>Name</h6>
                    <input class="form-control"  type="text">

                    <span class="p-3"></span>

                    <h6>Category</h6>
                    <select class="custom-select" id="inlineFormCustomSelectPref">
                        <option selected>Choose...</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>

                    <br>

                    <h6 class="pt-4">Another One</h6>
                    <input class="form-control"  type="text">

                    <span class="p-3"></span>

                    <h6>And Again</h6>
                    <input class="form-control"  type="text">

                    <span class="p-3"></span>

                    <h6>And Finally</h6>
                    <input class="form-control"  type="text">

                    <br>

                    <button type="submit" class="btn btn-primary">Add</button>

                </div>
            </div>


        </form>

    </section>
@endsection