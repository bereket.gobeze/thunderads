@extends('layouts.app')

@section('content')

    <section>
        <span style="float: right; position: relative"><a href="#" class="font-weight-bold h5 mb-0"><i class="far fa-plus-square"></i> ADD</a> </span>

        <h2>My Contents</h2>


        <div class="row mx-auto">

            <div class="col-md-3 p-1">
                <div class="card border-primary bg-transparent content-card" style="min-height: 8rem;">
                    <div class="card-body">
                        <span class="card-title h3">Gebeta</span>
                    </div>

                    <div class="card-footer text-right mb-0 bg-primary text-white">
                        <span class="h4 my-0"><i class="fab fa-android"></i></span>
                    </div>

                </div>
            </div>

            <div class="col-md-3 p-1">
                <div class="card border-primary bg-transparent content-card" style="min-height: 8rem;">
                    <div class="card-body">
                        <span class="card-title h3">Merkato Shopping</span>
                    </div>

                    <div class="card-footer text-right mb-0 bg-primary text-white">
                        <span class="h4 my-0"><i class="fab fa-telegram"></i></span>
                    </div>

                </div>
            </div>

            <div class="col-md-3 p-1">
                <div class="card border-primary bg-transparent content-card" style="min-height: 8rem;">
                    <div class="card-body">
                        <span class="card-title h3">Betting Blog</span>
                    </div>

                    <div class="card-footer text-right mb-0 bg-primary text-white">
                        <span class="h4 my-0"><i class="fas fa-globe"></i></span>
                    </div>

                </div>
            </div>

            <div class="col-md-3 p-1">
                <div class="card border-primary bg-transparent content-card" style="min-height: 8rem;">
                    <div class="card-body">
                        <span class="card-title h3">Genzeb</span>
                    </div>

                    <div class="card-footer text-right mb-0 bg-primary text-white">
                        <span class="h4 my-0"><i class="fab fa-android"></i></span>
                    </div>

                </div>
            </div>

            <div class="col-md-3 p-1">
                <div class="card border-primary bg-transparent content-card" style="min-height: 8rem;">
                    <div class="card-body">
                        <span class="card-title h3">Kana</span>
                    </div>

                    <div class="card-footer text-right mb-0 bg-primary text-white">
                        <span class="h4 my-0"><i class="fab fa-telegram"></i></span>
                    </div>

                </div>
            </div>

        </div>


    </section>

@endsection