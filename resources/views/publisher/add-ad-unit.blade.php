@extends('layouts.app')

@section('content')
    <section>
        <h2>Add Ad Unit</h2>
        <span class="text-small text-muted">For [Content Name]</span>

        <form>
            <span class="p-3"></span>

            <h4>Name</h4>
            <input class="form-control w-50"  type="text">

            <span class="p-3"></span>

            <h4>Type</h4>
            <input class="form-control w-50"  type="text">

            <span class="p-3"></span>

            <h4>Another Property</h4>
            <input class="form-control w-50"  type="text">

            <span class="p-3"></span>

            <h4>One More</h4>
            <input class="form-control w-50"  type="text">

            <br>

            <button type="submit" class="btn btn-primary">Add</button>

        </form>


    </section>
@endsection