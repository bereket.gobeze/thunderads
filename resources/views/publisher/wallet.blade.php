@extends('layouts.app')

@section('content')
    <section>
        <h2>Your Wallet</h2>

        <div class="card bg-transparent" style="min-height: 4rem;">
            <div class="card-body">
                <h3 class="card-title">Earnings</h3>

                <span class="">[Some text some text some text.]</span>
                <span class="float-right h3">Br 100.00</span>
                <br>
                <a href="#" class="btn btn-primary font-weight-bold mt-4">Request Payout</a>
            </div>
        </div>

        <hr>

        <h4>Transactions</h4>

        <table class="table table-hover" style="width: 99%">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Date</th>
                <th scope="col">Amount</th>
                <th scope="col">Status</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Aug 20, 2019</td>
                <td>Br 100.00</td>
                <td>Completed</td>
            </tr>
            <tr>
                <th scope="row">1</th>
                <td>Jun 20, 2019</td>
                <td>Br 956.00</td>
                <td>Canceled</td>
            </tr>
            <tr>
                <th scope="row">1</th>
                <td>Sept 20, 2019</td>
                <td>Br 100.00</td>
                <td>Pending</td>
            </tr>
            </tbody>
        </table>

    </section>
@endsection