@extends('layouts.app')

@section('content')

    <section>

        <h2 class="mb-2">Gebeta <span class="h3 mb-0"><i class="fab fa-android"></i></span></h2>

        <div class="card p-3 bg-secondary text-white">
            <h3 class="card-title">Earnings Summary</h3>
            <div class="card-body row">
                <div class="col-3">
                    <span class="h3">Br 0.00</span>
                    <br>Today
                </div>

                <div class="col-3">
                    <span class="h3">Br 0.00</span>
                    <br>This Week
                </div>

                <div class="col-3">
                    <span class="h3">Br 0.00</span>
                    <br>Month
                </div>

                <div class="col-3">
                    <span class="h3">Br 0.00</span>
                    <br>Year
                </div>
            </div>
        </div>


        <h3 class="mt-3">Ad Units</h3>

        <span style="display: inline; float: right; position: relative"><a href="#" class="font-weight-bold h5 mb-0"><i class="far fa-plus-square"></i> ADD</a> </span>

        <div class="row">

            <div class="col-md-3 p-1">
                <div class="card">
                    <div class="card-header text-center py-4 bg-success">
                        <span class="text-white font-weight-bold h2 mb-0">Text Ad</span>
                    </div>
                    <div class="card-body">
                        <span class="card-title">Bottom Banner</span>
                    </div>
                </div>

            </div>

            <div class="col-md-3 p-1">
                <div class="card">
                    <div class="card-header text-center py-4 bg-primary">
                        <span class="text-white font-weight-bold h2 mb-0">Image Ad</span>
                    </div>
                    <div class="card-body">
                        <span class="card-title">Side Image Ad</span>
                    </div>
                </div>

            </div>

            <div class="col-md-3 p-1">
                <div class="card">
                    <div class="card-header text-center py-4 bg-success">
                        <span class="text-white font-weight-bold h2 mb-0">Text Ad</span>
                    </div>
                    <div class="card-body">
                        <span class="card-title">Bottom Banner</span>
                    </div>
                </div>

            </div>

        </div>

    </section>

@endsection