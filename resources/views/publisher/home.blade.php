@extends('layouts.app')

@section('content')
    <div class="p-3">
        <section>
            <h2 class="font-weight-bold">Home</h2>

            <div class="card p-3 bg-secondary text-white">
                <h2 class="card-title font-weight-bold">Earnings Summary</h2>
                <div class="card-body row">
                    <div class="col-3">
                        <span class="h3">Br 0.00</span>
                        </br>Today
                    </div>

                    <div class="col-3">
                        <span class="h3">Br 0.00</span>
                        </br>This Week
                    </div>

                    <div class="col-3">
                        <span class="h3">Br 0.00</span>
                        </br>Month
                    </div>

                    <div class="col-3">
                        <span class="h3">Br 0.00</span>
                        </br>Year
                    </div>
                </div>
            </div>
        </section>

        <section class="mt-2">

            <h3>Performance Summary</h3>

            <div class="card-columns">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Apps</h5>
                        <p class="card-text">This is where the graphs and stuff goes...</p>
                    </div>
                </div>
                <div class="card p-3">
                    <blockquote class="blockquote mb-0 card-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                        <footer class="blockquote-footer">
                            <small class="text-muted">
                                Someone famous in <cite title="Source Title">Source Title</cite>
                            </small>
                        </footer>
                    </blockquote>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div>
                </div>
                <div class="card bg-primary text-white text-center p-3">
                    <blockquote class="blockquote mb-0">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat.</p>
                        <footer class="blockquote-footer">
                            <small>
                                Someone famous in <cite title="Source Title">Source Title</cite>
                            </small>
                        </footer>
                    </blockquote>
                </div>
            </div>

            <iframe width="310" height="610" scrolling="no" style="border: 0;" src="http://homestead.test/ad">

            </iframe>
        </section>

    </div>
@endsection