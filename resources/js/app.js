
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import Vuex from 'vuex';

// Use Vue Router
Vue.use(VueRouter);

// User Vuex
Vue.use(Vuex);


/**
 * Routing related code goes here
 */
// Define our routes
const routes = [
    //{ path: '/a', component: require('./components/AppContainer').default},
    { path: '/login', component: require('./components/auth/Login').default, meta: {user: 'guest'}},
    { path: '/register', component: require('./components/auth/Register').default, meta: {user: 'guest'}},

    // Publisher Routes
    { path: '/publisher/home', component: require('./components/publisher/Home').default, meta: {user: 'publisher'}},
    { path: '/publisher/my-contents/new', component: require('./components/publisher/AddContent').default, meta: {user: 'publisher'}},
    { path: '/publisher/my-contents/:content/ad-units/new', component: require('./components/publisher/AddAdUnit').default, meta: {user: 'publisher'}},
    { path: '/publisher/my-contents/:content/ad-units/:ad/edit', component: require('./components/publisher/EditAdUnit').default, meta: {user: 'publisher'}},
    { path: '/publisher/my-contents/:content/ad-units/:ad', component: require('./components/publisher/AdUnit').default, meta: {user: 'publisher'}},
    { path: '/publisher/my-contents/:id/edit', component: require('./components/publisher/EditContent').default, meta: {user: 'publisher'}},
    { path: '/publisher/my-contents/:id', component: require('./components/publisher/Content').default, meta: {user: 'publisher'}},
    { path: '/publisher/my-contents', component: require('./components/publisher/MyContents').default, meta: {user: 'publisher'}},
    { path: '/publisher/wallet', component: require('./components/publisher/Wallet').default, meta: {user: 'publisher'}},
    { path: '/publisher/profile', component: require('./components/publisher/Profile').default, meta: {user: 'publisher'}},
    { path: '/publisher/withdraw-request', component: require('./components/publisher/WithdrawRequest').default, meta: {user: 'publisher'}},

    // Advertiser Routes
    { path: '/advertiser/home', component: require('./components/advertiser/Home').default, meta: {user: 'advertiser'}},
    { path: '/advertiser/my-ads/new', component: require('./components/advertiser/CreateAd').default, meta: {user: 'advertiser'}},
    { path: '/advertiser/my-ads/:id/edit', component: require('./components/advertiser/EditAd').default, meta: {user: 'advertiser'}},
    { path: '/advertiser/my-ads/:id', component: require('./components/advertiser/Ad').default, meta: {user: 'advertiser'}},
    { path: '/advertiser/my-ads', component: require('./components/advertiser/MyAds').default, meta: {user: 'advertiser'}},
    { path: '/advertiser/profile', component: require('./components/advertiser/Profile').default, meta: {user: 'advertiser'}},
    { path: '/advertiser/wallet', component: require('./components/advertiser/Wallet').default, meta: {user: 'advertiser'}},

    // Admin Routes
    { path: '/admin', component: require('./components/admin/Home').default, meta: {user: 'admin'}},
    { path: '/admin/profile', component: require('./components/admin/Profile').default, meta: {user: 'admin'}},
    { path: '/admin/withdraw-requests', component: require('./components/admin/WithdrawRequests').default, meta: {user: 'admin'}},
    { path: '/admin/advertiser-deposit', component: require('./components/admin/AdvertiserDeposit').default, meta: {user: 'admin'}},

    // Index Route
    { path: '/', component: require('./components/Index').default, meta: {user: 'guest'}},

];

// Create the Vue router
const router = new VueRouter({
    routes // short for 'routes: routes'
});

// Navigation guards
router.beforeEach((to, from, next) => {
    // Get the logged in user type
    let userType = null;

    if (store.state.user_type) {
        userType = store.state.user_type;
        handleRoute(to, from, next, userType);
    } else {
        getUserType().then(userType => {
            handleRoute(to, from, next, userType);
        });
    }

});

async function getUserType() {
    let userType;
    try {
        let response = await axios.get('/api/user/type');
        if (response.data) {
            // Store the user_type in the Vuex store
            store.commit('setUserType', response.data);
            userType = response.data;
            return userType;
        }
    } catch (error) {
        if (error.response) {
            if (error.response.status === 401) {
                // User is unauthorized
                // Log out the user
                localStorage.setItem('access_token', null);
                localStorage.setItem('refresh_token', null);
                store.commit('setUserType', "guest");
                userType = "guest";
                return userType;
            }
        }
    }

    return userType;
}

function handleRoute(to, from, next, userType) {
    if(to.matched.some(record => record.meta.user === "publisher")) {
        // This route requires the logged in user type to be publisher
        if (userType === 'guest') {
            // Redirect to login page
            next({ path: '/login' });
        } else if(userType === 'advertiser') {
            // Redirect to advertiser home page
            next({ path: '/advertiser/home' });
        } else if (userType === 'admin') {
            // Redirect to admin home page
            next({ path: '/admin' });
        } else {
            next();
        }
    } else if (to.matched.some(record => record.meta.user === "guest")) {
        // This route requires the user not to be logged in
        if (userType === 'publisher') {
            // Redirect to publishers home page
            next({ path: '/publisher/home' });
        } else if(userType === 'advertiser') {
            // Redirect to advertiser home page
            next({ path: '/advertiser/home' });
        } else if (to.matched.some(record => record.meta.user === "advertiser")) {
            // This route requires the logged in user type to be advertiser
            if (userType === 'guest') {
                // Redirect to login page
                next({ path: '/login' });
            } else if(userType === 'publisher') {
                // Redirect to publisher home page
                next({ path: '/publisher/home' });
            } else {
                next();
            }
        } else if (userType === 'admin') {
            // Redirect to admin home page
            next({ path: '/admin' });
        } else {
            next();
        }
    } else if(to.matched.some(record => record.meta.user === "admin")) {
        if (userType === 'guest') {
            // Redirect to login page
            next({ path: '/login' });
        } else if(userType === 'advertiser') {
            // Redirect to advertiser home page
            next({ path: '/advertiser/home' });
        } else if (userType === 'publisher') {
            // Redirect to publisher home page
            next({ path: '/publisher/home' });
        } else {
            next();
        }

    } else {
        next();
    }
}

/**
 * Vuex related code goes here
 */
// Initiate vuex
const store = new Vuex.Store({
    state: {
        user_type: null,
    },
    mutations: {
        setUserType (state, userType) {
            state.user_type = userType;
        },
    },
    getters: {
        getUserType: (state) => {
            return state.user_type;
        }
    }
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('nav-bar', require('./components/layouts/NavBar.vue').default);
Vue.component('side-bar', require('./components/layouts/SideBar.vue').default);

/**
 * Set up defaults for axios
 */
axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem('access_token', null);
axios.defaults.headers.post['Accept'] = "application/json";

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    store,
    data: {
        one: false,
    },
    methods: {
         checkUserType: function() {
            // Check if the user type state is set
            if (localStorage.getItem('access_token', null)) {
                if (!this.$store.state.user_type) {
                    axios.get('/api/user/type')
                        .then(response => {
                            console.log(response.data);
                            if (response.data) {
                                // Store the user_type in the Vuex store
                                this.$store.commit('setUserType', response.data);
                            }
                        })
                        .catch(error => {
                            if (error.response) {
                                if (error.response.status === 401) {
                                    // User is unauthorized
                                    // Log out the user
                                    localStorage.setItem('access_token', null);
                                    localStorage.setItem('refresh_token', null);
                                }
                            }
                        })
                        .then(e => {
                        });

                }
            } else {
                this.$store.commit('setUserType', 'guest');
            }
        }
    },
    mounted() {
        this.checkUserType();
    },
    components: [
        'example-component',
        'side-bar',
        'nav-bar'
    ]
});
