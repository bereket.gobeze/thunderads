<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('vue');
});

Route::post('/telegram/bot', 'TelegramBotController@webHook');

// Temporary routes to test views

Route::get('/publisher/home', function() {
    return view('publisher.home');
});

Route::get('/publisher/my-contents', function() {
    return view('publisher.my-contents');
});

Route::get('publisher/content', function() {
    return view('publisher.content');
});

Route::get('publisher/content/new', function() {
    return view('publisher.add-content');
});

Route::get('publisher/content/one/ad-units/new', function() {
    return view('publisher.add-ad-unit');
});

Route::get('publisher/wallet', function() {
    return view('publisher.wallet');
});

Route::get('publisher/profile', function() {
    return view('publisher.profile');
});


Route::get('/advertiser/home', function() {
    return view('advertiser.home');
});

Route::get('advertiser/ads/new', function() {
    return view('advertiser.create-ad');
});

Route::get('advertiser/my-ads', function() {
    return view('advertiser.my-ads');
});

Route::get('advertiser/ads/1', function() {
    return view('advertiser.ad');
});

Route::get('advertiser/ads/1/edit', function() {
    return view('advertiser.edit-ad');
});

Route::get('advertiser/profile', function() {
    return view('advertiser.profile');
});

Route::get('advertiser/wallet', function() {
    return view('advertiser.wallet');
});

Route::get('signin', function() {
    return view('auth.new-login');
});

Route::get('signup', function() {
    return view('auth.new-register');
});

Route::get('ads/preview/text', 'AdController@textAdPreview');
Route::get('ads/get', 'AdController@getAd');
Route::get('ads/click', 'AdController@clickAd');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
