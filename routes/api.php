<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'Api\UserAuthentication@login');
Route::post('/change-password', 'Api\UserAuthentication@changePassword')->middleware('auth:api');

Route::get('/user/type', 'Api\UserAuthentication@userType')->middleware('auth:api');
Route::get('/user', 'Api\UserAuthentication@getUserInfo')->middleware('auth:api');

// Publisher Routes
Route::prefix('publisher')->middleware('auth:api')->group(function() {
    // Ad Units
    Route::get('/content/{content}/adunit', 'Api\PublisherController@getAdUnits');
    Route::post('/content/{content}/ad-units/{adUnit}/edit', 'Api\PublisherController@editAdUint');
    Route::get('/content/{content}/ad-units/{adUnit}', 'Api\PublisherController@getAdUnit');
    Route::post('/content/{content}/adunit/new', 'Api\PublisherController@createAdUnit');

    // Contents
    Route::get('/content', 'Api\PublisherController@getContents');
    Route::post('/content/{content}/edit', 'Api\PublisherController@editContent');
    Route::get('/content/{content}', 'Api\PublisherController@getContent');
    Route::post('/content/new', 'Api\PublisherController@createContent');

    // Withdraw Requests
    Route::post('/withdraw-request/new', 'Api\PublisherController@createWithdrawRequest');

    // Wallet
    Route::get('/wallet', 'Api\PublisherController@walletDetails');

    // Home
    Route::get('/', 'Api\PublisherController@home');
});

// Advertiser Routes
Route::prefix('advertiser')->middleware('auth:api')->group(function() {
    // Ads
    Route::get('/ads', 'Api\AdvertiserController@getAds');
    Route::post('/ads/{ad}/edit', 'Api\AdvertiserController@editAd');
    Route::get('/ads/{ad}', 'Api\AdvertiserController@getAd');
    Route::post('/ads/new', 'Api\AdvertiserController@createAd');

    // Remaining balance
    Route::get('/remaining-balance', 'Api\AdvertiserController@getRemainingBalance');

    // Home
    Route::get('/', 'Api\AdvertiserController@home');
});

// Admin Routes
Route::prefix('admin')->middleware('auth:api')->group(function() {
    // Withdraw Requests
    Route::get('/withdraw-requests/pending', 'Api\AdminController@getPendingWithdrawRequests');
    Route::post('/withdraw-requests/{withdrawRequest}/accept', 'Api\AdminController@acceptWithdrawRequest');
    Route::post('/withdraw-requests/{withdrawRequest}/reject', 'Api\AdminController@rejectWithdrawRequest');

    // Dashboard
    Route::get('/dashboard', 'Api\AdminController@getDashboardStats');

    // Deposit To Advertiser
    Route::post('/advertiser/deposit', 'Api\AdminController@depositToAdvertiser');
});